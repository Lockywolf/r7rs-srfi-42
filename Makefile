.PHONY: all
all: package
package: srfi-42.tgz
srfi-42.tgz:
	snow-chibi package "srfi/42.sld"

clean:
	rm -rf srfi-42.tgz
