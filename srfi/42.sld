;Time-stamp: <2019-10-26 20:06:36 lockywolf>
#;(Written-by Lockywolf gmail.com)
#;(Year 2019)
#;(This file blindly loads the reference implementation.)
(define-library (srfi 42)
  (import (scheme r5rs) (srfi 23))
  (export do-ec list-ec append-ec string-ec string-append-ec vector-ec
	  vector-of-length-ec sum-ec product-ec min-ec max-ec any?-ec
	  every?-ec first-ec last-ec fold-ec fold3-ec : :list :string
	  :vector :integers :range :real-range :char-range :port
	  :dispatched :do :let :parallel :while :until )
  (include "42/ec.scm"))
